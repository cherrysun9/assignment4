echo "This is the deploy step"

export PROJECT_ID=assignment4-312318
gcloud config set project $PROJECT_ID
export HELLOWORLD_COMPUTE_ZONE=us-central1-b


gcloud container clusters get-credentials testcluster1

kubectl delete deployment hello-world-deployment || echo "hello-world-deployment deployment does not exist"
kubectl delete service hello-world-deployment || echo "hello-world-deployment service does not exist"
kubectl delete ingress hello-world-ingress || echo "hello-world-ingress does not exist"
kubectl delete service hello-world-service || echo "hello-world-service does not exist"

kubectl create -f hello-world-deployment.yaml
kubectl expose deployment hello-world-deployment --target-port=5000 --type=NodePort

kubectl apply -f hello-world-ingress.yaml

echo "Done deploying"
